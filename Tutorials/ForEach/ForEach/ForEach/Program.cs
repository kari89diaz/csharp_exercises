﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ForEach
{
    class Program
    {
        static void Main(string[] args)
        {
            string[] nombres = { "Luis", "Daniel", "María", "Rosa", "Santiago" };
            foreach(string name in nombres)
            {
                Console.WriteLine("Hola " + name);
            }
            Console.ReadKey();
        }
    }
}
