﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SentenciaIf
{
    class Program
    {
        static void Main(string[] args)
        {
            if(10 < 5)
            {
                Console.WriteLine("La sentencia es verdadera");
            }
            else
            {
                Console.WriteLine("La sentencia es falsa");
            }
            Console.ReadKey();
        }
    }
}
