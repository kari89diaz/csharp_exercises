﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Matrices
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] edades = { 18, 19, 20 };
            int[,] arregloBi = new int[4, 4];
            
            //Llenar Matriz
            for (int x = 0; x < 4; x++)
            {
                for (int i = 0; i < 4; i++)
                {
                    arregloBi[x, i] = 100;
                }
            }

            //Mostrar Matriz
            for (int x = 0; x < 4; x++)
            {
                for (int i = 0; i < 4; i++)
                {
                    Console.Write(arregloBi[x, i]+" ");
                }
                Console.WriteLine(" ");
            }
            Console.ReadKey();
            
        }
    }
}
