﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Array
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] arregloEnteros = { 0, 0, 0, 0, 0 }; // no tiene tamáño definido
            //int[] arregloInt = new int[5]; // en esta se le indica el tamaño
            arregloEnteros[0] = 45;
            arregloEnteros[1] = 4;
            arregloEnteros[2] = 39;
            arregloEnteros[3] = 23;
            arregloEnteros[4] = 9;
            Console.WriteLine("Valor {0}", arregloEnteros[4]);
            Console.ReadKey();
        }
    }
}
