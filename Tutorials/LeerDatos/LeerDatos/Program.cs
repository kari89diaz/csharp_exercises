﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LeerDatos
{
    class Program
    {
        static void Main(string[] args)
        {
            string entrada = "";
            Console.WriteLine("Escribe tu nombre:");
            entrada = Console.ReadLine();
            Console.WriteLine("Hola {0}. ¿Cómo te encuentras hoy?", entrada);
            Console.ReadKey();
        }
    }
}
