﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CicloDoWhile
{
    class Program
    {
        static void Main(string[] args)
        {
            int num = 0;
            do
            {
                num++;
                Console.WriteLine("Hola mundo " +num);
            } while (num < 10);
        }
    }
}
