﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HolaMundo //namespace -> Paquete, contiene el bloque de código que está entre las llaves. Se pueden tener varios para distintas tareas. En este caso esa System (using System arriba). 
    //Para usar un namespace agregamos using HolaMundo (en este caso sería innecesario ya que estamos en el mismo archivo)
{
    class Program
    {
        static void Main(string[] args) //función principal
        {
            //variables -> int (números enteros) - char (caracteres) - string (cadena o texto) - float - bool
            int numero = 20;

            //instrucciones
            Console.Write("La variable número contiene " +numero);
            Console.ReadKey();
        }
    }
}
