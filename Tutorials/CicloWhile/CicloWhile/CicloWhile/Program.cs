﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CicloWhile
{
    class Program
    {
        static void Main(string[] args)
        {
            int numero = 0;
            while (numero <= 10) 
            {
                numero++;
                Console.WriteLine(numero);
            }
        }
    }
}
