﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VariablesConsola
{
    class Program
    {
        static void Main(string[] args)
        {
            int entero = 5;
            string cadena = "C#";
            bool valor = false;

            Console.WriteLine("La variable entera tiene "+entero +" La variable cadena contiene " +cadena +" La variable valor es " +valor);
            Console.WriteLine("La variable entera tiene {0} La variable cadena contiene {1} La variable valor es {2}", entero, cadena, valor);
            Console.ReadKey();
        }
    }
}
