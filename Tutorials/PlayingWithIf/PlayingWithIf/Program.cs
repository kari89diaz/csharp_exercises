﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SentenciaIf
{
    class Program
    {
        static void Main(string[] args)
        {
            int years = 0;
            Console.WriteLine("Ingrese su edad");
            years = int.Parse(Console.ReadLine());
            if (years >= 18)
            {
                Console.WriteLine("This person is an adult");
                Console.ReadKey();
            }
            else
            {
                Console.WriteLine("This person is a child");
                Console.ReadKey();
            }
        }
    }
}